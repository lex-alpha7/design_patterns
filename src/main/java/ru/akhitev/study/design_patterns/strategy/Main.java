package ru.akhitev.study.design_patterns.strategy;

import ru.akhitev.study.design_patterns.strategy.ducks.*;
import ru.akhitev.study.design_patterns.strategy.fly.FlyWithWings;
import ru.akhitev.study.design_patterns.strategy.quack.ExoticQuack;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<AbstractDuck> ducks = new ArrayList<AbstractDuck>();
        ducks.add(new ExoticDuck());
        ducks.add(new SimpleDuck());
        ducks.add(new WoodenDuck());
        ducks.add(new RubberDuck());

        for (AbstractDuck duck : ducks)
        {
            duck.display();
            duck.swim();
            duck.quack();
            duck.fly();
            System.out.println();
        }

        AbstractDuck upgradableDuck = new UpgradableDuck();
        upgradableDuck.display();
        upgradableDuck.swim();
        upgradableDuck.quack();
        upgradableDuck.fly();

        upgradableDuck.setFlyBehaviour(new FlyWithWings());
        upgradableDuck.setQuackBehaviour(new ExoticQuack());
        upgradableDuck.quack();
        upgradableDuck.fly();
    }
}
