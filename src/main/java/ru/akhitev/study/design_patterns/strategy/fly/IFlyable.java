package ru.akhitev.study.design_patterns.strategy.fly;

/**
 * Created by hitev on 04.09.14.
 */
public interface IFlyable {
    void fly();
}
