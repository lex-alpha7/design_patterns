package ru.akhitev.study.design_patterns.strategy.ducks;

import ru.akhitev.study.design_patterns.strategy.fly.NoFly;
import ru.akhitev.study.design_patterns.strategy.quack.NoQuack;

/**
 * Created by hitev on 04.09.14.
 */
public class UpgradableDuck extends AbstractDuck {
    public UpgradableDuck()
    {
        flyBehaviour = new NoFly();
        quackBehaviour = new NoQuack();
    }
    @Override
    public void display() {
        System.out.println("I'm an upgradable duck!");
    }
}
