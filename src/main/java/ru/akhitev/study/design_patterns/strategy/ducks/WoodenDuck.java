package ru.akhitev.study.design_patterns.strategy.ducks;

import ru.akhitev.study.design_patterns.strategy.fly.NoFly;
import ru.akhitev.study.design_patterns.strategy.quack.NoQuack;

public class WoodenDuck extends AbstractDuck {
    public WoodenDuck()
    {
        flyBehaviour = new NoFly();
        quackBehaviour = new NoQuack();
    }

    @Override
    public void display() {
        System.out.println("Hi! I'm a wooden duck!");
    }
}
