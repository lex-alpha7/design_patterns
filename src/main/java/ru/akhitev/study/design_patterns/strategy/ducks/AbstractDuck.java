package ru.akhitev.study.design_patterns.strategy.ducks;

import ru.akhitev.study.design_patterns.strategy.fly.FlyWithWings;
import ru.akhitev.study.design_patterns.strategy.fly.IFlyable;
import ru.akhitev.study.design_patterns.strategy.quack.IQuackable;
import ru.akhitev.study.design_patterns.strategy.quack.SimpleQuack;

/**
 * Created by hitev on 04.09.14.
 */
public abstract class AbstractDuck {
    protected IFlyable flyBehaviour;
    protected IQuackable quackBehaviour;

    public AbstractDuck()
    {
        flyBehaviour = new FlyWithWings();
        quackBehaviour = new SimpleQuack();
    }

    public void setQuackBehaviour(IQuackable newQuackBehaviour)
    {
        quackBehaviour = newQuackBehaviour;
    }

    public void setFlyBehaviour(IFlyable newFlyBehaviour)
    {
        flyBehaviour = newFlyBehaviour;
    }

    public void swim()
    {
        System.out.println("I'm swimming");
    }

    public void quack()
    {
        quackBehaviour.quack();
    }

    public void fly()
    {
        flyBehaviour.fly();
    }

    public abstract void display();
}
