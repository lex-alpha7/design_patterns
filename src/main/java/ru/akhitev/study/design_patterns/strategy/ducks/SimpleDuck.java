package ru.akhitev.study.design_patterns.strategy.ducks;

/**
 * Created by hitev on 04.09.14.
 */
public class SimpleDuck extends AbstractDuck {
    @Override
    public void display() {
        System.out.println("Hi! I'm a simle duck.");
    }
}
