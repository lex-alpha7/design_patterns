package ru.akhitev.study.design_patterns.strategy.ducks;

import ru.akhitev.study.design_patterns.strategy.fly.NoFly;

public class RubberDuck extends AbstractDuck {
    public RubberDuck()
    {
        flyBehaviour = new NoFly();
    }
    @Override
    public void display() {
        System.out.println("Hi! I'm a rubber duck!");
    }
}
