package ru.akhitev.study.design_patterns.strategy.fly;

/**
 * Created by hitev on 04.09.14.
 */
public class NoFly implements IFlyable {
    @Override
    public void fly()
    {
        System.out.println("---");
    }
}
