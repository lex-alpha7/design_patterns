package ru.akhitev.study.design_patterns.strategy.fly;

/**
 * Created by hitev on 04.09.14.
 */
public class FlyWithWings implements IFlyable {
    @Override
    public void fly() {
        System.out.println("I'm flying with my wings");
    }
}
