package ru.akhitev.study.design_patterns.strategy.ducks;

import ru.akhitev.study.design_patterns.strategy.quack.ExoticQuack;

/**
 * Created by hitev on 04.09.14.
 */
public class ExoticDuck extends AbstractDuck {
    public ExoticDuck()
    {
        quackBehaviour = new ExoticQuack();
    }
    @Override
    public void display() {
        System.out.println("Hi! I'm an exotic duck.");
    }
}
