package ru.akhitev.study.design_patterns.strategy.quack;

/**
 * Created by hitev on 04.09.14.
 */
public interface IQuackable {
    void quack();
}
