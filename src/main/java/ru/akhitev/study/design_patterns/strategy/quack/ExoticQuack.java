package ru.akhitev.study.design_patterns.strategy.quack;

/**
 * Created by hitev on 04.09.14.
 */
public class ExoticQuack implements IQuackable {
    @Override
    public void quack() {
        System.out.println("Meaow! Meaow!");
    }
}
