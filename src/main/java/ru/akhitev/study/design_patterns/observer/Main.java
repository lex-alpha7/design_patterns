package ru.akhitev.study.design_patterns.observer;

import ru.akhitev.study.design_patterns.observer.news.NewsAgregator;

/**
 * Created by hitev on 04.09.14.
 */
public class Main {
    public static void main(String[] args) {
        NewsAgregator newsAgregator = new NewsAgregator();
        newsAgregator.newNewsAvailable();
        System.out.println();
        newsAgregator.newNewsAvailable();
        System.out.println();
    }
}
