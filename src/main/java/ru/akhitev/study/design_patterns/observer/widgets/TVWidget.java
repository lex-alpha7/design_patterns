package ru.akhitev.study.design_patterns.observer.widgets;

/**
 * Created by hitev on 04.09.14.
 */
public class TVWidget {
    public void update(String twitter, String lenta, String tv){
        System.out.println("TV: "+tv);
    }
}
