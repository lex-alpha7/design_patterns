package ru.akhitev.study.design_patterns.observer.news;

import ru.akhitev.study.design_patterns.observer.widgets.LentaWidget;
import ru.akhitev.study.design_patterns.observer.widgets.TVWidget;
import ru.akhitev.study.design_patterns.observer.widgets.TwitterWidget;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by hitev on 04.09.14.
 */
public class NewsAgregator {

    private static TwitterWidget twitterWidget;
    private static TVWidget tvWidget;
    private static LentaWidget lentaWidget;
    private static Random random;

    public NewsAgregator(){
        twitterWidget = new TwitterWidget();
        tvWidget = new TVWidget();
        lentaWidget = new LentaWidget();
        random = new Random();
    }

    public String getTwitterNews(){
        List<String> news = new ArrayList<String>();
        news.add("Новость из Twitter 1");
        news.add("Новость из Twitter 2");
        news.add("Новость из Twitter 3");
        return news.get(random.nextInt(3));
    }

    public String getTvNews(){
        List<String> news = new ArrayList<String>();
        news.add("Новость из TV 1");
        news.add("Новость из TV 2");
        news.add("Новость из TV 3");
        return news.get(random.nextInt(3));
    }

    public String getLentaNews(){
        List<String> news = new ArrayList<String>();
        news.add("Новость из Lenta 1");
        news.add("Новость из Lenta 2");
        news.add("Новость из Lenta 3");
        return news.get(random.nextInt(3));
    }

    /**
     * вызывается при поступлении любой новости
     */
    public void newNewsAvailable(){

        // получаем новости
        String twitter = getTwitterNews();
        String tv = getTvNews();
        String lenta = getLentaNews();

        // обновляем виджеты
        twitterWidget.update(twitter, lenta, tv);
        lentaWidget.update(twitter, lenta, tv);
        tvWidget.update(twitter, lenta, tv);
    }
}
